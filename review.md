# Review

## Info

Reviewer Name: Hariharan Sezhiyan
Paper Title: seL4: Formal Verification of an OS Kernel

## Summary

### Problem and why we care

Operating system kernels today are large, complex beasts that have bugs that are (1) hard to catch and (2) can potentially cause the kernel to catch. Moreso, these same kernels are increasing being deployed in safety critical systems like personal automobiles: we simply cannot tolerate bugs in such cases.

### Gap in current approaches

Existing solutions include unit and integration testing, which although good, are ultimately only as effective as the tests humans write. Other solutions like memory/type safe languages (e.g. Rust) and static checking only target specific issues (like memory) but fail to address other ways kernels crash (e.g. whether an algorithm will produce a infinite loop).

### Hypothesis, Key Idea, or Main Claim

The key idea is to build a microkernel, called seL4, from the top-down with verification in mind. This is accomplished by providing a development environment in which OS developers first write the kernel in Haskell, a higher level language, which is more amenable to being verified against an abstract representation. Then, developers reimplement said kernel in C for performance. After every iteration, the kernel is verified to ensure it exactly follows the specification.

### Method for Proving the Claim

The authors use a specific type of verification called a refinement proof which ensures that an artifact (in this case, either the Haskell or C kernel implementation) exactly follows a specification (in this case, either the abstract or executable specification). The authors use the Isabelle higher-order theorem prover.

### Method for evaluating

The authors evaluate whether the performance of the kernel is acceptable by measuring the number of CPU cycles taken for IPC communication. IPC communication is the most critial performance component in a microkernel because all interactions occur through IPC. The authors also evaluate the time it took to develop the kernel to justify the design process they propose.

### Contributions: what we take away

Verification can indeed be used for OS kernel development, but in order for it to work, certain features in C cannot be used or should be restricted (e.g. the & operator in local functions, excessive use of global variables, etc). Thus, although promising since verification guarantees an absence of bugs (assuming the initial spec is free of bugs), it mainly should be used for smaller systems and incorporated into larger systems in specific submodules.

## Pros (3-6 bullets)
- Provides a clean, iterative design environment for OS developers
- Guarantees that the behavior of the final kernel exactly follows a predefined spec 
- Because of the above pro, a lot of bugs can be root caused by looking at the spec instead of having to dig into the kernel (which is much larger and more complex)

## Cons (3-6 bullets)
- This does not catch all bugs, it only ensures the final kernel follows the behavior of the initial spec. If a bug exists in the spec, it will make its way into the final kernel.
- It places some restrictions on C language use, which can be bad since existing systems may make extensive use of such features.
- It doesn't address issues across the stack: seL4 assumes that the compiler, hardware, boot code, and assembly code are free of bugs.

### What is your analysis of the proposed?

The paper is academically valuable because it proposes a new way to design OS kernels. Instead of micro-optimizing one feature of kernels, it focuses on a new way to think about the problem. Although it does have drawbacks, the ideas in the paper are still relevant today as research on using verification to improve the reliability of systems is still a very active field of research.

## Details Comments, Observations, Questions

I found their analysis of the time it took to develop the kernel quite insightful: instead of keeping this as just an academic project, the authors are pushing their vision that a verification-focused kernel can be practical in industry. As a former engineer in industry, I found this analysis refreshing and interesting to read.











